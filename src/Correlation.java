import java.util.*;

public class Correlation {
    private int amount = 100;
    private int t = 1;
    private double step = (double) t / (double) amount;
    private double a1 = 1;
    private double a2 = 0.5;
    private double q1 = 0;
    private double q2 = Math.PI;

    //Генерація вибірки A
    public Map<Double, Double> generateSequenceA(double step) {
        Map<Double, Double> sequenceA = new HashMap<>();

        for (double i = 0; i <= t; i += step) {
            double y = a1 * Math.sin((2 * Math.PI / t) * i + q1);
            sequenceA.put(i, y);
        }

        return sequenceA;
    }

    //Генерація вибірки В
    public Map<Double, Double> generateSequnceB(Map<Double, Double> sequenceA) {
        Map<Double, Double> sequenceB = new HashMap<>();

        List<Double> aKeys = new ArrayList<>();
        aKeys.addAll(sequenceA.keySet());

        for (int i = 0; i < sequenceA.size(); i++) {
            double x = aKeys.get(i);
            double y = a2 * Math.sin((2 * Math.PI / t) * x + q2);
            sequenceB.put(x, y);
        }

        return sequenceB;
    }

    //Генерація вибірки С
    public Map<Double, Double> generateSequenceC(Map<Double, Double> sequenceA) {
        List<Double> normalDistribution = generateNormalDistribution(a1);

        List<Double> aKeys = new ArrayList<>();
        aKeys.addAll(sequenceA.keySet());

        Map<Double, Double> sequenceC = new HashMap<>();

        for (int i = 0; i < sequenceA.size(); i++) {
            double x = aKeys.get(i);
            double y = sequenceA.get(aKeys.get(i)) + normalDistribution.get(i);
            sequenceC.put(x, y);
        }

        return sequenceC;
    }

    //Генерація нормального розподілу
    private List<Double> generateNormalDistribution(double ampl){
        double a = ampl * 0.25;
        double x;
        double max = a / 2.0;
        double mean = 0.0;

        List<Double> array = new ArrayList<>(this.amount);

        Random rnd = new Random();

        double stddev = max / 3.000;

        for (int i = 0; i < this.amount; i++)
        {
            double u1 = rnd.nextDouble();
            double u2 = rnd.nextDouble();

            double rndNormal = Math.sqrt(-2.0 * Math.log(u1)) *
                    Math.sin(2.0 * Math.PI * u2);
            x = (rndNormal * stddev) + mean;
            array.add(x);
        }
        Collections.sort(array);

        return array;
    }

    //Обчислення кореляційного критерію Пірсона
    public double evaluatePirson(Map<Double, Double> sequenceA, Map<Double, Double> sequenceB) {
        double xAver = calculateAverage(sequenceA);
        double yAver = calculateAverage(sequenceB);
        double xyAver = calculateAverage(sequenceA, sequenceB);
        double xSigma = calculateStandartDeviation(sequenceA);
        double ySigma = calculateStandartDeviation(sequenceB);

        return (xyAver - xAver * yAver) / (xSigma * ySigma);
    }

    //Обчислення середнього для 1 вибірки
    private double calculateAverage(Map<Double, Double> sequence) {
        List<Double> keys = new ArrayList<>();
        keys.addAll(sequence.keySet());

        double sum = 0.0;
        for (int i = 0; i < sequence.size(); i++) {
            sum += sequence.get(keys.get(i));
        }

        return sum / sequence.size();
    }

    //Обчислення середнього для 2 вибірок
    private double calculateAverage(Map<Double, Double> sequence1, Map<Double, Double> sequence2) {
        List<Double> keys1 = new ArrayList<>();
        keys1.addAll(sequence1.keySet());

        List<Double> keys2 = new ArrayList<>();
        keys2.addAll(sequence2.keySet());

        double sum = 0.0;
        for (int i = 0; i < sequence1.size(); i++) {
            sum += sequence1.get(keys1.get(i)) * sequence2.get(keys2.get(i));
        }

        return sum / sequence1.size();
    }

    //Середнє квадратичне відхилення
    private double calculateStandartDeviation(Map<Double, Double> sequence) {
        List<Double> keys = new ArrayList<>();
        keys.addAll(sequence.keySet());

        double average = calculateAverage(sequence);
        double sum = 0.0;
        for (int i = 0; i < sequence.size(); i++) {
            sum += Math.pow(sequence.get(keys.get(i)) - average, 2);
        }

        return Math.sqrt(sum / sequence.size());
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public double getStep() {
        return step;
    }

    public void setStep(double step) {
        this.step = step;
    }

    public double getA1() {
        return a1;
    }

    public void setA1(double a1) {
        this.a1 = a1;
    }

    public double getA2() {
        return a2;
    }

    public void setA2(double a2) {
        this.a2 = a2;
    }

    public double getQ1() {
        return q1;
    }

    public void setQ1(double q1) {
        this.q1 = q1;
    }

    public double getQ2() {
        return q2;
    }

    public void setQ2(double q2) {
        this.q2 = q2;
    }
}
