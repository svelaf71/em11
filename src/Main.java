import java.util.Map;

public class Main {


    public static void main(String... args) {
        Correlation correlation = new Correlation();
        Map<Double, Double> sequenceA = correlation.generateSequenceA(correlation.getStep());
        Map<Double, Double> sequenceB = correlation.generateSequnceB(sequenceA);
        Map<Double, Double> sequenceC = correlation.generateSequenceC(sequenceA);

        System.out.println(sequenceA);
        System.out.println(sequenceB);
        System.out.println(sequenceC);

        System.out.println("\n\nКореляція для А і В: " + correlation.evaluatePirson(sequenceA, sequenceB));
        System.out.println("Кореляція для А і С: " + correlation.evaluatePirson(sequenceA, sequenceC));
    }
}
